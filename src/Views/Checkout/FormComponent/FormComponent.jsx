import React from "react";

import { validationError } from "./Valiate";
import TextField from "./TextField";
import "./FormComponent.scss";
import ServicePay from "../../../Services/ServicePay";

class FormComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      formControls: {
        number: "",
        name: "",
        validity: "",
        cvv: "",
        parcel: "",
      },
      errors: {},
      installments: [
        "1x R$12.000,00 sem juros",
        "4x R$3.000,00 sem juros",
        "8x R$1.500,00 sem juros",
        "12x R$1.000,00 sem juros",
      ],
    };
  }

  changeHandler = (event) => {
    const name = event.target.name;
    const value = event.target.value;
    const formControls = {
      ...this.state.formControls,
      [name]: value,
    };
    this.setState({ formControls });
    this.props.form(formControls);
  };

  onSelectParcel = (value) => {
    const formControls = {
      ...this.state.formControls,
      parcel: value,
    };
    this.setState({ formControls });
  };

  toggleCard = (rotateCard) => {
    this.props.rotate(rotateCard);
  };

  handleSubmit = async (e) => {
    e.preventDefault();
    const { formControls } = this.state;
    const errors = validationError(formControls);

    if (errors) {
      this.setState({ errors });
      return false;
    } else {
      ServicePay.pay(formControls, (response) => {
        console.log(response);
      });
    }
  };

  render() {
    const { errors } = this.state;

    return (
      <form className="form" onSubmit={this.handleSubmit}>
        <div className="row">
          <div
            className={`form-field form-field--input ${
              errors.number ? "form-field--invalid" : ""
            }`}
          >
            <TextField
              name="number"
              id="number"
              autoComplete="off"
              onChange={this.changeHandler}
              mask="card"
            />
            <label className="form-field--label" htmlFor="number">
              Número do cartão
            </label>
            {errors.number && (
              <span className="form-field--error">
                Número de cartão inválido
              </span>
            )}
          </div>
        </div>

        <div className="row">
          <div
            className={`form-field form-field--input ${
              errors.name ? "form-field--invalid" : ""
            }`}
          >
            <TextField
              name="name"
              id="name"
              autoComplete="off"
              onChange={this.changeHandler}
            />
            <label className="form-field--label" htmlFor="name">
              Nome (igual ao cartão)
            </label>
            {errors.name && (
              <span className="form-field--error">
                Insira seu nome completo
              </span>
            )}
          </div>
        </div>

        <div className="row">
          <div
            className={`form-field form-field--input ${
              errors.validity ? "form-field--invalid" : ""
            }`}
          >
            <TextField
              name="validity"
              id="validity"
              autoComplete="off"
              onChange={this.changeHandler}
              mask="date"
            />
            <label className="form-field--label" htmlFor="validity">
              Validade
            </label>
            {errors.validity && (
              <span className="form-field--error">Data inválida</span>
            )}
          </div>
          <div
            className={`form-field form-field--input ${
              errors.cvv ? "form-field--invalid" : ""
            }`}
          >
            <TextField
              name="cvv"
              id="cvv"
              autoComplete="off"
              maxLength="3"
              onChange={this.changeHandler}
            />
            <label className="form-field--label" htmlFor="cvv">
              CVV{" "}
              <i
                onMouseEnter={() => this.toggleCard(true)}
                onMouseOut={() => this.toggleCard(false)}
              >
                i
              </i>
            </label>
            {errors.cvv && (
              <span className="form-field--error">Código inválido</span>
            )}
          </div>
        </div>

        <div className="row">
          <div
            className={`form-field form-field--input ${
              errors.parcel ? "form-field--invalid" : ""
            }`}
          >
            <input
              type="text"
              value={this.state.formControls.parcel}
              name="parcel"
              id="parcel"
              autoComplete="off"
              readOnly="readonly"
            />
            <label className="form-field--label" htmlFor="parcel">
              Número de parcelas
            </label>
            <ul className="dropdown">
              {this.state.installments.map((installment) => (
                <li
                  key={installment}
                  onClick={() => this.onSelectParcel(installment)}
                >
                  {installment}
                </li>
              ))}
            </ul>
            {errors.parcel && (
              <span className="form-field--error">
                Insira o número de parcelas
              </span>
            )}
          </div>
        </div>

        <div className="row">
          <button className="btn btn__red">Continuar</button>
        </div>
      </form>
    );
  }
}

export default FormComponent;
