import React from "react";

class TextField extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: "",
    };
  }

  card(value) {
    return value
      .replace(/\D/g, "")
      .replace(/(\d{4})(\d)/, "$1 $2")
      .replace(/(\d{4})(\d)/, "$1 $2")
      .replace(/(\d{4})(\d{1,2})/, "$1 $2")
      .replace(/(\d{4})\d/, "$1");
  };

  date(value) {
    return value
      .replace(/\D/g, "")
      .replace(/(\d{2})(\d)/, "$1/$2")
      .replace(/(\d{2})\d/, "$1");
  }

  handleChange = (e) => {
    const value = this.props.mask ? this[this.props.mask](e.target.value) : e.target.value;
    if (value !== this.state.value) {
      this.setState({ value });
      this.props.onChange(e);
    }
  };

  render() {
    return (
      <input
        {...this.props}
        type="text"
        value={this.state.value}
        onChange={this.handleChange}
      />
    );
  }
}

export default TextField;
