export function validationError(fields) {
  const errors = {};

  if (!testCard(fields["number"])) {
    errors["number"] = true;
  } else {
    delete errors["number"];
  }

  if (!fields["name"]) {
    errors["name"] = true;
  } else {
    delete errors["name"];
  }

  if (!fields["cvv"]) {
    errors["cvv"] = true;
  } else {
    delete errors["cvv"];
  }

  if (testDate(fields["validity"])) {
    errors["validity"] = true;
  } else {
    delete errors["validity"];
  }

  if (!fields["parcel"]) {
    errors["parcel"] = true;
  } else {
    delete errors["parcel"];
  }

  return Object.entries(errors).length === 0 ? false : errors;
}

function testDate(value) {
  const date = value.replace(/\D/g, "");
  if(!date && new Date(`20${date.substr(2)}`, date.substr(0,2) - 1, 1) <= new Date()) {
    return true;
  } else {
    return null
  }
}

function testCard(num) {
  const cards = {
    Mastercardc: "5[1-5][0-9]{14}",
    Visa: "4(?:[0-9]{12}|[0-9]{15})",
    Amex: "3[47][0-9]{13}",
    DinersClub: "3(?:0[0-5][0-9]{11}|[68][0-9]{12})",
    Discover: "6011[0-9]{12}",
    JCB: "(?:3[0-9]{15}|(2131|1800)[0-9]{11})",
  };
  const card = num.replace(/\D/g, "")
  for (let value in cards) {
    if (card.match(cards[value])) {
      return value;
    }
  }
  return false;
}