import React from "react";

import BoxCardComponent from "../BoxCardComponent/BoxCardComponent";
import FormComponent from "../FormComponent/FormComponent";
import BreadcrumbComponent from "../BreadcrumbComponent/BreadcrumbComponent";
import HeaderComponent from "../HeaderComponent/HeaderComponent";
import ShoppingCart from "../ShoppingCart/ShoppingCart";

import "./Cart.scss";

class Cart extends React.Component {
  state = {
    rotateCard: false,
    formControls: null
  };
  
  receivesRotation = (rotateCard) => {
    this.setState({ rotateCard });
  };

  receiveForm = (formControls) => {
    this.setState({ formControls });
  }

  render() {
    return (
      <div>
        <HeaderComponent />
        <div className="container">
          <div className="content">
            <BoxCardComponent
              rotateCard={this.state.rotateCard}
              form={this.state.formControls}
            />
            <section className="section-form">
              <BreadcrumbComponent />

              <FormComponent
                rotate={this.receivesRotation}
                form={this.receiveForm}
              />
            </section>
          </div>
          <ShoppingCart />
        </div>
      </div>
    );
  }
}

export default Cart;
