### Running the local project

* git clone git clone https://rafa88@bitbucket.org/rafa88/bexs-front-end-exam.git
* run `bexs-front-end-exam` to enter the folder
* run `npm install`
* run `npm start`
* Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
